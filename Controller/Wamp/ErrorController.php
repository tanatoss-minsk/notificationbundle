<?php

namespace Garant\ECM\Bundle\NotificationBundle\Controller\Wamp;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ErrorController
 * @package Garant\ECM\Bundle\NotificationBundle\Controller\Wamp
 */
class ErrorController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function errorExceptionAction()
    {
        return new JsonResponse();
    }

    /**
     * @return JsonResponse
     */
    public function resourceNotFoundExceptionAction()
    {
        return new JsonResponse();
    }
}