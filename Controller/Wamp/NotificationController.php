<?php

namespace Garant\ECM\Bundle\NotificationBundle\Controller\Wamp;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee;
use Garant\ECM\Bundle\NotificationBundle\Wamp\Socket\MessageInterface;
use Garant\ECM\Bundle\NotificationBundle\Wamp\NotificationServerInterface;
use Garant\ECM\Bundle\APIBundle\Traits\ResponseTrait;
use Ratchet\Wamp\Topic;
use Ratchet\Wamp\WampConnection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class NotificationController
 * @package Garant\ECM\Bundle\NotificationBundle\Controller\Wamp
 */
class NotificationController extends Controller
{
    use ResponseTrait;

    /**
     * @param Request $request
     * @param MessageInterface $message
     * @param Topic $topic
     * @return JsonResponse
     */
    public function notificationAction(Request $request, MessageInterface $message, Topic $topic, NotificationServerInterface $server)
    {
        /**
         * Online Employees
         *
         * @var WampConnection $connection
         */
        $onlineEmployees = array();
        foreach($server->getOnlineUsers() as $user){
            $onlineEmployees[] = $user->getId();
        }
        $em =   $this->getDoctrine()->getManager();
        $qBuilder = $em->getRepository('GarantECMNotificationBundle:Notification')
            ->createQueryBuilder('notification')
            ->addSelect('notificationEmployee')
            ->join('notification.notificationEmployees', 'notificationEmployee')
            ->join('notificationEmployee.employee', 'employee')
            ->where('notification.id IN (:notification_id)')
            ->setParameter('notification_id', $message->getContent())
            ->andWhere('employee.id IN (:online_employees)')
            ->setParameter('online_employees', $onlineEmployees)
            ->andWhere('notificationEmployee.status = :pending_status')
            ->setParameter('pending_status', NotificationEmployee::STATUS_PENDING)
        ;

        /**
         * @var Notification $notification
         */
        $notification = $qBuilder->getQuery()->getSingleResult();
        if(!$notification){
            return $this->response(null);
        }
        $notificationEmployees = $notification->getNotificationEmployees();

        /**
         * @var NotificationEmployee $notificationEmployee
         */
        foreach ($notificationEmployees as $notificationEmployee) {
            if(!empty($notification->getConfig()['need_confirm'])){
                continue;
            }
            $notificationEmployee->setStatus(NotificationEmployee::STATUS_SENT);
            $notificationEmployee->setProcessDate(new \DateTime());
            $em->persist($notificationEmployee);
        }
        $em->flush();
        return $this->prepareResponse($notification);
    }

    /**
     * @param $notification
     * @return JsonResponse
     */
    private function prepareResponse(Notification $notification)
    {
        $html = null;
        if(!empty($notification->getConfig()['template'])){
            try {
                $html = $this->get('templating')->render($notification->getConfig()['template'], [
                    'subject' => $notification->getSubject()
                ]);
                $notification->setConfig(array_merge($notification->getConfig(), ['message_html' => $html]));
            } catch (\Exception $exception) {

                echo $exception->getMessage();
            }
        }
        return $this->response($notification);
    }
}