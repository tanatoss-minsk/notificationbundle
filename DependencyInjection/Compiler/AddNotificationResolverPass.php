<?php

namespace Garant\ECM\Bundle\NotificationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use \Garant\ECM\Bundle\NotificationBundle\Notification\ResolverBroker;

/**
 * Class AddNotificationResolverPass
 * @package Garant\ECM\Bundle\NotificationBundle\DependencyInjection\Compiler
 */
class AddNotificationResolverPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if ( $container->hasDefinition('garant_ecm_notification.resolver') ) {
            $definitionNotificationResolver = $container->getDefinition('garant_ecm_notification.resolver');
            $definitionNotificationListener = $container->getDefinition('garant_ecm_notification.notification_listener');

            foreach ( $container->findTaggedServiceIds('notification.resolver') as $id => $tags ) {
                foreach ($tags as $tag) {
                    $notification = $tag['notification'];
                    $definitionNotificationResolver->addMethodCall('addResolver', array(new Reference($id), $notification));
                }
            } // foreach
            $container->setDefinition('garant_ecm_notification.resolver', $definitionNotificationResolver);
            foreach ($container->getParameter('garant_ecm_notification.events_parameters') as $eventParam) {
                $definitionNotificationListener->addTag('kernel.event_listener', ['event'=> $eventParam['event'], 'method' =>  'onNotify']);
            } //foreach
        }
    }
}