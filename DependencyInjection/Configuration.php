<?php

namespace Garant\ECM\Bundle\NotificationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    const ROOT_NAME = 'garant_ecm_notification';

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root(self::ROOT_NAME);

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $rootNode
            ->children()
                ->scalarNode('notification_entity_class')->isRequired()->end()
                ->scalarNode('employee_entity_class')->isRequired()->end()
                ->arrayNode('events_parameters')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('event')->end()
                            ->scalarNode('type')->defaultValue('info')->end()
                            ->scalarNode('ttl')->defaultValue(2000)->end()
                            ->booleanNode('need_confirm')->defaultFalse()->end()
                            ->scalarNode('template')->defaultValue('null')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
