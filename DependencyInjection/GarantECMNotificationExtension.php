<?php

namespace Garant\ECM\Bundle\NotificationBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

/**
 * Class GarantECMNotificationExtension
 * @package Garant\ECM\Bundle\NotificationBundle\DependencyInjection
 */
class GarantECMNotificationExtension extends Extension implements PrependExtensionInterface
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('notifications.yml');

//        // Add path for routing file locator
        $fileLocatorDefinition = $container->getDefinition('garant_ecm_notification.routing.file_locator');
        $fileLocatorDefinition->addArgument(array(__DIR__ . '/../Resources/config/routing'));

        $container->setParameter('garant_ecm_notification.notification_entity_class', $config['notification_entity_class']);
        $container->setParameter('garant_ecm_notification.employee_entity_class', $config['employee_entity_class']);
        $container->setParameter('garant_ecm_notification.events_parameters', $config['events_parameters']);
    }

    /**
     * Without this fake interface service doctrine.orm.listeners.resolve_target_entity won't be tagged as doctrine.event_listener tag in DoctrineExtension
     * @see https://github.com/doctrine/DoctrineBundle/issues/330
     *
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        $config = [
            'orm' => [
                'resolve_target_entities' => [
                    'SomeFakeInterface' => 'SomeFakeEntity',
                ]
            ]
        ];

        $container->prependExtensionConfig('doctrine', $config);
    }
}
