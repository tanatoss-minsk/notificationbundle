<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity\Repository;

use Doctrine\ORM\QueryBuilder;
use Garant\ECM\Bundle\DocumentBundle\Interfaces\EmployeeInterface;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee as Employee;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\ORM\EntityRepository;

class NotificationRepository  extends EntityRepository
{
    public function getNotificationByUser(Employee $employee, $count = 10, $status = null)
    {
        $qb = $this->createQueryBuilder('notification')
            ->addSelect('notificationEmployee')
            ->join('notification.notificationEmployees', 'notificationEmployee')
            ->join('notificationEmployee.employee', 'employee')
            ->andWhere('employee = :employee')
            ->setParameter(':employee', $employee)
            ->setMaxResults($count)
            ->orderBy('notification.createdAt', 'desc')
            ;
        if($status){
            $qb->andWhere('notificationEmployee.status = :pending_status')
                ->setParameter('pending_status', $status);
        }
        return $qb->getQuery()->getResult();
    }
}