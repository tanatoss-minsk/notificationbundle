<?php

namespace Garant\ECM\Bundle\NotificationBundle\Event;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EventCollector
 * @package Garant\ECM\Bundle\NotificationBundle\Event
 */
class EventCollector
{
    /**
     * @var array
     */
    protected $events = [];

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * EventCollector constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $eventParams = $container->getParameter('garant_ecm_notification.events_parameters');
        foreach ($eventParams as $eventParam){
            $this->events[$eventParam['event']] = $eventParam;
        }
    }

    /**
     * @param $serviceId
     * @param $tags
     */
    public function addEvent($serviceId, $tags)
    {
        if(isset($tags['event'])){
            $this->events[$tags['event']] = array_merge($tags, ['service' => $serviceId]);
        }
    }

    /**
     * @param $eventName
     * @return mixed
     */
    public function getEvent($eventName)
    {
        if(isset($this->events[$eventName])){
            return $this->events[$eventName];
        }
    }
}