<?php

namespace Garant\ECM\Bundle\NotificationBundle\Event;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Symfony\Component\EventDispatcher\GenericEvent;
use Garant\ECM\Bundle\NotificationBundle\Event\NotificationEventInterface;

/**
 * Class UnsentNotificationEvent
 * @package Garant\ECM\Bundle\NotificationBundle\Event
 */
class UnsentNotificationEvent extends GenericEvent implements \JsonSerializable
{
    const NAME = 'ecm.notification.unsent';

    /**
     * @param Notification $notification
     * @param array $arguments
     */
    public function __construct(Notification $notification, array $arguments = array())
    {
        parent::__construct($notification, $arguments);
    }

    public static function getName()
    {
        return self::NAME;
    }

    public function jsonSerialize()
    {
        return [
            'subject' => $this->getSubject()
        ];
    }
}