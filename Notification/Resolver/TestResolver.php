<?php

namespace Garant\ECM\Bundle\NotificationBundle\Notification\Resolver;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee;
use Garant\ECM\Bundle\NotificationBundle\Notification\ResolverInterface;
use Garant\ECM\Bundle\NotificationBundle\Event\NotificationEventInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class TestResolver
 * @package Garant\ECM\Bundle\NotificationBundle\Notification\Resolver
 */
class TestResolver implements ResolverInterface
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry  = $registry;
    }

    /**
     * @param Notification $notification
     * @return Notification|false
     */
    public function resolve(Event $event, Notification $notification)
    {
        $notification->createNotificationEmployee($event->getSubject());
        return $notification;
    }
}