<?php

namespace Garant\ECM\Bundle\NotificationBundle\Notification\Resolver;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Notification\ResolverInterface;
use Garant\ECM\Bundle\NotificationBundle\Event\NotificationEventInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UnsentNotificationResolver
 * @package Garant\ECM\Bundle\NotificationBundle\Notification\Resolver
 */
class UnsentNotificationResolver implements ResolverInterface
{
    /**
     * @param Notification $notification
     * @return Notification|false
     */
    public function resolve(Event $event, Notification $notification)
    {
        if ($notification->getSubject() instanceof Notification) {

            $notification = $notification->getSubject();
        }

        return $notification;
    }
}