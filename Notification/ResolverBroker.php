<?php

namespace Garant\ECM\Bundle\NotificationBundle\Notification;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ResolverBroker
 * @package Garant\ECM\Bundle\NotificationBundle\Notification
 */
class ResolverBroker implements ResolverInterface
{
    /**
     * @var array
     */
    protected $resolvers = array();

    static $events = array();

    /**
     * @param ResolverInterface $resolver
     * @param $notificationTag
     * @return $this
     */
    public function addResolver(ResolverInterface $resolver, $notificationTag)
    {
        $this->resolvers[$notificationTag][] = $resolver;
        return $this;
    }

    /**
     * @param Notification $notification
     * @return Notification
     */
    public function resolve(Event $event, Notification $notification)
    {
        if(!isset($this->resolvers[$notification->getEvent()])){
            throw new \RuntimeException(sprintf('%s need resolver service. Read documentation.', get_class($event)));
        }
        /**
         * @var ResolverInterface $resolver
         */
        foreach ($this->resolvers[$notification->getEvent()] as $resolver) {
            $notification = $resolver->resolve($event, $notification);
        }

        return $notification;
    }
}