/**
 * Конфиг галпа для вендорного бандла нотификаций
 * @type {{}}
 */

var path = require('path');

var pathToConfig = './vendor/garant/ecm-notification-bundle/Resources/';

module.exports = {
    app: {
        baseName: 'GarantNotification'
    },
    js: {
        baseDir: [pathToConfig + 'src/js'],
        src: [ pathToConfig + 'src/js/garant-notification.js']
    },
    buildLocations: {
        js: pathToConfig + 'public/js/'
    }
};