<?php

namespace Garant\ECM\Bundle\NotificationBundle\Tests;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Tests\MessageEnum;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

/**
 * Class NotificationTest
 * @package Garant\ECM\Bundle\NotificationBundle\Tests
 *
 * Не включать транзакции!
 */
class NotificationFunctionalTest extends WebTestCase
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var
     */
    private $em;

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $port;

    /**
     * @var string
     */
    protected $hostSelenium = 'http://localhost:4444/wd/hub';

    protected $container;

    /**
     * @var BaseEmployee
     */
    protected $user;

    const USER_PROPERTY = 'superAdmin';

    /**
     * @var DesiredCapabilities
     */
    protected static $capabilities;

    public function setUp()
    {
        parent::setUp();
        static::bootKernel();
        self::$capabilities = DesiredCapabilities::firefox();

        shell_exec('ps aux | grep -ie notification:server | awk \'{print $2}\' | xargs kill -9 > /dev/null &');
        shell_exec('php /home/tanatoss/www/garantecm/bin/console  garant:ecm:notification:server --env=test   > /dev/null &');

        $this->container = static::$kernel->getContainer();
        $this->doctrine = static::$kernel
            ->getContainer()
            ->get('doctrine');
        $this->doctrine->getConnection()->beginTransaction();
        $this->em = $this->doctrine->getManager();
        $this->host = static::$kernel
            ->getContainer()
            ->getParameter('garant_ecm_notification.server.default_host');
        $this->port = static::$kernel
            ->getContainer()
            ->getParameter('garant_ecm_notification.server.default_port');
    }

    public function tearDown()
    {
        parent::tearDown();
        shell_exec('ps aux | grep -ie notification:server | awk \'{print $2}\' | xargs kill -9> /dev/null &');
        shell_exec('php /home/tanatoss/www/garantecm/bin/console  garant:ecm:notification:server> /dev/null &');
    }

    public function _testRatchetRun()
    {
        $testRatchetHost = $this->createDriver($this->host.':'.$this->port);
        $title = $testRatchetHost->getTitle();
        /**
         *  If Ratchet don't work, return title = Problem loading page or other
         */
        $this->assertEmpty($title);
        $testRatchetHost->close();
    }

    /**
     *  route garant_ecm_notification_test_send create DemoNotificationEvent,
     *  which sends a message to the user specified in the subject
     */
    public function _testNotification()
    {
        $context = $this->container->get('router')->getContext();
        $context->setHost($this->host);
        $context->setScheme('http');
        $context->setBaseUrl('/app_test.php');
        $this->getEmployee();
        $route = $this->container->get('router')->generate('garant_ecm_notification_test', ['id' => $this->user->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $testHost = $this->createDriver($route);
        $testHost->wait(1);
        $this->authorization($testHost);
        $connectionConatainer = $testHost->findElement(
            WebDriverBy::id(MessageEnum::CONNECTION_CONTAINER)
        );
        $this->assertEquals($connectionConatainer->getText(), MessageEnum::MESSAGE_CONNECT_SUCCESS);

        $routeSend = $this->container->get('router')->generate('garant_ecm_notification_test_send', ['id' => $this->user->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $sendHost = $this->createDriver($routeSend);
        $testHost->wait(1);
        $this->authorization($sendHost);
        $resultUserId = $testHost->findElement(
            WebDriverBy::id(MessageEnum::SUBSCRIBER_DATA_ID)
        )->getText();
        $this->assertNotEmpty($resultUserId);
        $this->assertEquals($resultUserId, $this->user->getId());
        $sendHost->close();
        $testHost->close();
        $notificationEmployee = $this->doctrine->getRepository(NotificationEmployee::class)->findOneBy(['employee'=>$this->user]);
        $this->assertNotEmpty($notificationEmployee);
        $notification = $notificationEmployee->getNotification();
        $this->assertNotEmpty($notification);
        $this->em->remove($notificationEmployee);
        $this->em->remove($notification);
        $this->em->remove($this->user);
        $this->em->flush();
    }

    /**
     * @param RemoteWebDriver $driver
     */
    private function authorization(RemoteWebDriver $driver)
    {
        $driver->findElement(
            WebDriverBy::id('username')
        )->sendKeys(self::USER_PROPERTY);
        $driver->findElement(
            WebDriverBy::id('password')
        )->sendKeys(self::USER_PROPERTY);
        $link = $driver->findElement(
            WebDriverBy::id('_submit')
        );
        $link->click();
        $driver->wait(1);
    }

    /**
     * @param $host
     * @return RemoteWebDriver
     */
    private function createDriver($host)
    {
        $driver = RemoteWebDriver::create($this->hostSelenium, self::$capabilities, 5000);
        $driver->get($host);
        return $driver;
    }

    private function getEmployee()
    {
        if(!$this->user = $this->doctrine->getRepository(
            $this->container->getParameter('garant_ecm_notification.employee_entity_class')
        )->findOneBy(['username' => self::USER_PROPERTY])){
            $userClassName = $this->container->getParameter('garant_ecm_notification.employee_entity_class');
            $this->user = new $userClassName();
            $this->user->setUsername(self::USER_PROPERTY);
            $this->user->setPlainPassword(self::USER_PROPERTY);
            $this->user->setEmail('supertester@mail.com');
            $this->user->setEnabled(true);
            $this->user->setSuperAdmin(true);
            $this->user->setLocked(false);
            $this->em->persist($this->user);
            $this->em->flush();
        }
    }

}