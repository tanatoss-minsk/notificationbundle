<?php

namespace Garant\ECM\Bundle\NotificationBundle\Topic\Generator;

/**
 * Interface StrategyInterface
 * @package Garant\ECM\Bundle\NotificationBundle\Topic\Generator
 */
interface StrategyInterface
{
    /**
     * @return mixed
     */
    public function getRoute();

    /**
     * @return mixed
     */
    public function getParameters();
}