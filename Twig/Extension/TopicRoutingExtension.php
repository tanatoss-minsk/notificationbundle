<?php

namespace Garant\ECM\Bundle\NotificationBundle\Twig\Extension;

use Symfony\Bridge\Twig\Extension\RoutingExtension;

/**
 * Class TopicRoutingExtension
 * @package Garant\ECM\Bundle\NotificationBundle\Twig\Extension
 */
class TopicRoutingExtension extends RoutingExtension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('topic_url', array($this, 'getUrl'), array('is_safe_callback' => array($this, 'isUrlGenerationSafe'))),
            new \Twig_SimpleFunction('topic_path', array($this, 'getPath'), array('is_safe_callback' => array($this, 'isUrlGenerationSafe'))),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'topic_routing';
    }
}