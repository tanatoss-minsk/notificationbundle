<?php

namespace Garant\ECM\Bundle\NotificationBundle\Wamp;

use Garant\ECM\Bundle\NotificationBundle\Wamp\Socket\MessageInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Ratchet\Wamp\WampServerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Router;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Garant\ECM\Bundle\NotificationBundle\Wamp\NotificationServerInterface;

/**
 * Class Server
 * @package Garant\ECM\Bundle\NotificationBundle\Wamp
 */
class Server implements WampServerInterface, Socket\PushInterface, NotificationServerInterface
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var ControllerResolver
     */
    protected $resolver;

    /**
     * @var \SplObjectStorage
     */
    protected $subscribedTopics;

    /**
     * @var LoggerInterface
     */
    protected $logger;


    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var Employee
     */
    protected $callUser;

    protected $clients = [];

    /**
     * @var \SplObjectStorage
     */
    protected $onlineUsers;

    /**
     * @param Router $router
     * @param ControllerResolver $resolver
     */
    public function __construct(Router $router, ControllerResolver $resolver, LoggerInterface $logger, TokenStorageInterface $tokenStorage)
    {
        $this->router   = $router;
        $this->resolver = $resolver;
        $this->logger   = $logger;
        $this->tokenStorage = $tokenStorage;

        $this->subscribedTopics = new \SplObjectStorage;
        $this->onlineUsers = new \SplObjectStorage;
    }

    /**
     * @param MessageInterface $message
     * @return mixed|void
     */
    public function onPush(MessageInterface $message)
    {
        try {
            $topics = $message->getTopics();
            foreach ($this->subscribedTopics as $topic) {
                if (in_array($topic->getId(), $topics)) {
                    $parameters = $this->router->match($topic->getId());
                    $request = new Request(array('message' => $message, 'topic' => $topic), array(), $parameters);
                    $controller = $this->resolver->getController($request);
                    try {

                        $resolver = new OptionsResolver();
                        $resolver->setDefaults(array(
                            'message' => '',
                            'exclude' => array(),
                            'eligible' => array(),
                        ));
                        //Вызываем контролер, помечаем нотификации как прочитанные тем юзерам, что онлайн
                        $controllerOption = call_user_func($controller, $request, $message, $topic, $this);
                        if(!$controllerOption){
                            continue;
                        }
                        $this->logger->info(sprintf(__METHOD__ . ': %s', $controllerOption->getContent()));
                        $topic->broadcast($controllerOption->getContent());
                    } catch (\Exception $ex) {
                        $this->logger->warning($ex->getMessage(), array('exception' => $ex));
                    }
                }
            } // foreach
            return;
        } catch (\Exception $ex) {
            $this->logger->warning($ex->getMessage(), array('exception' => $ex));
        }
    }

    /**
     * An RPC call has been received
     * @param \Ratchet\ConnectionInterface $conn
     * @param string $id The unique ID of the RPC, required to respond to
     * @param string|Topic $topic The topic to execute the call against
     * @param array $params Call parameters received from the client
     */
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params, $call = null)
    {
        try {
            $parameters = $this->router->match($topic->getId());
            $request = new Request(['params' => $params], array(), $parameters);
            
            $this->callUser = $this->getUser($conn);
            
            $controller = $this->resolver->getController($request);
            $response = call_user_func($controller, $request, $this);
            $this->logger->info(sprintf(__METHOD__ . ': %s', $response->getContent()));
            $conn->callResult($id, $response->getContent());
        }
        catch(ResourceNotFoundException $e){
            $this->logger->error('Invalid URL: ' . $topic->getId());

            $response = $this->error('Invalid URL: ' . $topic->getId());
            $conn->callError($id, $response->getContent());
        }
    }

    /**
     * A request to subscribe to a topic has been made
     * @param \Ratchet\ConnectionInterface $conn
     * @param string|Topic $topic The topic to subscribe to
     */
    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        if (!$topic->has($conn)) {

            $topic->add($conn);
        }
        if (!$this->subscribedTopics->contains($topic)) {

            $this->subscribedTopics->attach($topic);
        }

        $topic = new Topic($this->router->generate('notification_check_unsent_notifications'));
        $this->onCall($conn, time(), $topic, [], true);
    }

    /**
     * A request to unsubscribe from a topic has been made
     * @param \Ratchet\ConnectionInterface $conn
     * @param string|Topic $topic The topic to unsubscribe from
     */
    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        if ($topic->has($conn)) {

            $topic->remove($conn);
        }

        if (!$topic->count()) {

            $this->subscribedTopics->detach($topic);
        }
    }

    /**
     * A client is attempting to publish content to a subscribed connections on a URI
     * @param \Ratchet\ConnectionInterface $conn
     * @param string|Topic $topic The topic the user has attempted to publish to
     * @param string $event Payload of the publish
     * @param array $exclude A list of session IDs the message should be excluded from (blacklist)
     * @param array $eligible A list of session Ids the message should be send to (whitelist)
     */
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        if (!$this->subscribedTopics->contains($topic)) {

            $this->subscribedTopics->attach($topic);
        }

        $topic->broadcast($event, $exclude, $eligible);
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    public function onOpen(ConnectionInterface $conn)
    {
        if(!$newUser = $this->getUser($conn)) return;

        foreach($this->onlineUsers as $user){
            if($user->getId() == $newUser->getId()){
                return;
            }
        }
        $this->onlineUsers->attach($newUser);
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    public function onClose(ConnectionInterface $conn)
    {
        if(!$lostUser = $this->getUser($conn)) return;

        foreach($this->onlineUsers as $user){
            if($user->getId() == $lostUser->getId()){
                $this->onlineUsers->detach($user);
            }
        }
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->logger->error($e->getMessage(), array('exception' => $e));

        try {

            $parameters = $this->router->match('/' . strtolower(get_class($e)));
        } catch (ResourceNotFoundException $exception) {

            $e = $exception;
            $parameters = $this->router->match($this->router->generate('error_resource_not_found_exception'));
        }

        $request = new Request(array('exception' => $e), array(), $parameters);

        /**
         * @var \Symfony\Component\HttpFoundation\Session\SessionInterface $session
         */
        $session = $conn->Session;
        $request->setSession($session);
        $controller = $this->resolver->getController($request);

        $response = call_user_func($controller, $request);

        $conn->send($response);
    }

    /**
     * @return Employee
     */
    public function getCallUser()
    {
        return $this->callUser;
    }

    /**
     * @return \SplObjectStorage
     */
    public function getOnlineUsers()
    {
        return $this->onlineUsers;
    }

    /**
     * @param ConnectionInterface $conn
     * @return Employee | null
     */
    private function getUser(ConnectionInterface $conn)
    {
        /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
        $session = $conn->Session;
        $token =  unserialize($session->get('_security_main'));

        $this->tokenStorage->setToken($token);
        if($this->tokenStorage->getToken()){
            return $this->tokenStorage->getToken()->getUser();
        }
    }
}