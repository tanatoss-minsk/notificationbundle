<?php

namespace Garant\ECM\Bundle\NotificationBundle\Wamp\Socket;

/**
 * Class Message
 * @package Garant\ECM\Bundle\NotificationBundle\Wamp\Socket
 */
class Message implements MessageInterface
{
    /**
     * @var string
     */
    protected $topics;

    /**
     * @var string
     */
    protected $content;

    /**
     * @param string $topics
     * @param string $content
     */
    public function __construct($topics, $content = null)
    {
        $this->topic = $topics;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getTopics()
    {
        return $this->topic;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}