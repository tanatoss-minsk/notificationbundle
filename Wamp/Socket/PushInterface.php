<?php

namespace Garant\ECM\Bundle\NotificationBundle\Wamp\Socket;

/**
 * Interface PushInterface
 * @package Garant\ECM\Bundle\NotificationBundle\Wamp\Socket
 */
interface PushInterface
{
    const PUSH_METHOD = 'onPush';

    /**
     * @param MessageInterface $message
     * @return mixed
     */
    public function onPush(MessageInterface $message);
}